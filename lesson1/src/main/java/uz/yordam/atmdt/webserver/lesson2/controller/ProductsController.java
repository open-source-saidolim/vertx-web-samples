package uz.yordam.atmdt.webserver.lesson2.controller;

import io.vertx.ext.web.RoutingContext;

public class ProductsController {

    public void getList(RoutingContext routingContext) {
        routingContext.response()
                .end("Product list:" +
                        " 1. Milk " +
                        " 2. Chocolate " +
                        " 3. Socks " +
                        " 4. .....");
    }
}
