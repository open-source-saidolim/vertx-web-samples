package uz.yordam.atmdt.webserver.lesson2.controller;

import io.vertx.ext.web.RoutingContext;

public class PlaneController {

    public void fly(RoutingContext routingContext){
        routingContext.response().end("I'm flying !!!");
    }

    public void landing(RoutingContext routingContext){
        routingContext.response().end("I'm landing !!!");
    }
}
