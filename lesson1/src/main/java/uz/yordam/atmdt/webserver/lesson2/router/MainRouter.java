package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import uz.yordam.atmdt.webserver.lesson2.controller.PlaneController;
import uz.yordam.atmdt.webserver.lesson2.controller.ProductsController;

public class MainRouter {

   private PlaneController planeController;
   private ProductsController productsController;

    public MainRouter() {
        planeController = new PlaneController();
        productsController = new ProductsController();
    }

    /**
     * Making new router
     * @param vertx
     * @return
     */
    public Router makeRouter(Vertx vertx){
        Router router = Router.router(vertx);

        router.route("/plane/fly").handler(planeController::fly);
        router.route("/plane/landing").handler(planeController::landing);

        router.route("/product/list").handler(productsController::getList);

        router.route().handler(this::defaultHandler);

        return router;
    }

    /**
     * if there is no route, run this one
     * @param routingContext
     */
    public void defaultHandler(RoutingContext routingContext){
        routingContext.response().end("Wrong way...");
    }
}
