package uz.yordam.atmdt.webserver.lesson2.controller;

import io.vertx.ext.web.RoutingContext;

public class GeneralController {

    protected String tableName = "General";

    public void getList(RoutingContext ctx) {
        ctx.response().end(tableName + " :: get List");
    }
    public void getById(RoutingContext ctx) {
        ctx.response().end(tableName + " :: getById");
    }
    public void save(RoutingContext ctx) {
        ctx.response().end(tableName + " :: save");
    }
    public void delete(RoutingContext ctx) {
        ctx.response().end(tableName + " :: delete");
    }
}
