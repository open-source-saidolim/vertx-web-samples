package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import uz.yordam.atmdt.webserver.lesson2.controller.StudentController;

public class StudentRouter extends GeneralRouter {

    public StudentRouter(Vertx vertx) {
        super(vertx);
        controller = new StudentController();
        mapping();
    }
}
