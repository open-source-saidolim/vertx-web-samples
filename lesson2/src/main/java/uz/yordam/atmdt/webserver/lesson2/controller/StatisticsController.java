package uz.yordam.atmdt.webserver.lesson2.controller;

import io.vertx.ext.web.RoutingContext;

public class StatisticsController extends GeneralController {

    public StatisticsController() {
        tableName = "Statictics";
    }

    public void studentCount(RoutingContext ctx) {
        ctx.response().end("Statistics :: Student Count");
    }
}
