package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import uz.yordam.atmdt.webserver.lesson2.controller.StudentController;
import uz.yordam.atmdt.webserver.lesson2.controller.TeacherController;

public class MainRouter {

    private TeacherController teacherController;
    private StudentController studentController;

    public MainRouter() {
        teacherController = new TeacherController();
        studentController = new StudentController();
    }

    /**
     * Making new router
     *
     * @param vertx
     * @return
     */
    public Router makeRouter(Vertx vertx) {
        Router router = Router.router(vertx);

//        router.route("/teacher/list").handler(teacherController::getList);
//        router.route("/teacher/byId").handler(teacherController::getById);
//        router.route("/teacher/save").handler(teacherController::save);
//        router.route("/teacher/delete").handler(teacherController::delete);


        router.mountSubRouter("/teacher", new TeacherRouter(vertx).getRouter());
        router.mountSubRouter("/student", new StudentRouter(vertx).getRouter());
        router.mountSubRouter("/kafedra", new KafedraRouter(vertx).getRouter());
        router.mountSubRouter("/statistics", new StatisticsRouter(vertx).getRouter());
        router.mountSubRouter("/monitoring", new MonitoringRouter(vertx).getRouter());


        router.route().handler(this::defaultHandler);

        return router;
    }

    /**
     * if there is no route, run this one
     *
     * @param routingContext
     */
    public void defaultHandler(RoutingContext routingContext) {
        routingContext.response()
                .setStatusCode(404)
                .end("Wrong way...");
    }
}
