package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import uz.yordam.atmdt.webserver.lesson2.controller.TeacherController;

public class TeacherRouter extends GeneralRouter {

    public TeacherRouter(Vertx vertx) {
        super(vertx);
        controller = new TeacherController();
        mapping();
    }

}
