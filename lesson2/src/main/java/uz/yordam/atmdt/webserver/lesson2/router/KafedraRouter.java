package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import uz.yordam.atmdt.webserver.lesson2.controller.KafedraController;

public class KafedraRouter extends GeneralRouter {

    public KafedraRouter(Vertx vertx) {
        super(vertx);
        controller = new KafedraController();
        mapping();
    }
}
