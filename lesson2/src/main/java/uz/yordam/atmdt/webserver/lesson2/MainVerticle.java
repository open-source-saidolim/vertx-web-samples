package uz.yordam.atmdt.webserver.lesson2;

import io.vertx.core.AbstractVerticle;
import io.vertx.ext.web.Router;
import uz.yordam.atmdt.webserver.lesson2.router.MainRouter;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start() {

        Router router = new MainRouter().makeRouter(vertx);

        vertx
                .createHttpServer()
                .requestHandler(router)
                .listen(8082);

    }
}
