package uz.yordam.atmdt.webserver.lesson2.controller;

import io.vertx.ext.web.RoutingContext;

public class MonitoringController extends GeneralController {

    public MonitoringController() {
        tableName = "Monitoring";
    }

    public void studentCount(RoutingContext ctx) {
        ctx.response().end("Monitoring :: Student Count");
    }
}
