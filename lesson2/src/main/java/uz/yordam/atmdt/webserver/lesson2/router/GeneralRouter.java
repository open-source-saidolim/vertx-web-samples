package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import io.vertx.ext.web.Router;
import uz.yordam.atmdt.webserver.lesson2.controller.GeneralController;

public class GeneralRouter {

    private Vertx vertx;
    private Router router;
    protected GeneralController controller;

    public GeneralRouter(Vertx vertx) {
        this.vertx = vertx;
        this.router = Router.router(vertx);
    }

    public Router getRouter() {
        return this.router;
    }

    protected void mapping() {
        router.route("/list").handler(controller::getList);
        router.route("/byId").handler(controller::getById);
        router.route("/save").handler(controller::save);
        router.route("/delete").handler(controller::delete);
    }
}
