package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import uz.yordam.atmdt.webserver.lesson2.controller.MonitoringController;

public class MonitoringRouter extends GeneralRouter {


    public MonitoringRouter(Vertx vertx) {
        super(vertx);
        controller = new MonitoringController();
        mapping();
    }

    @Override
    protected void mapping() {
        super.mapping();
        getRouter().route("/std")
                .handler(((MonitoringController) controller)::studentCount);
    }
}
