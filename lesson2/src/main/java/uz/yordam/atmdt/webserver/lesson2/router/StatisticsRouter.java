package uz.yordam.atmdt.webserver.lesson2.router;

import io.vertx.core.Vertx;
import uz.yordam.atmdt.webserver.lesson2.controller.StatisticsController;

public class StatisticsRouter extends GeneralRouter {


    public StatisticsRouter(Vertx vertx) {
        super(vertx);
        controller = new StatisticsController();
        mapping();
    }

    @Override
    protected void mapping() {
        getRouter().route("/std").handler(((StatisticsController)controller)::studentCount);
    }
}
